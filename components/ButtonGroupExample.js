import React, {useState} from 'react';
import {View, StyleSheet} from 'react-native';
import {ButtonGroup, Icon} from 'react-native-elements';

export default App => {
  const buttons = ['segment 1', 'segment 2', 'segment 3', 'segment 4', 'segment 5'];
  const [selectedIndex, setSelectedIndex] = useState('');

  const component1 = () => (
    <Icon name="home" color={selectedIndex === 0 ? '#fff' : '#000'} />
  );
  const component2 = () => (
    <Icon name="room" color={selectedIndex === 1 ? '#fff' : '#000'} />
  );
  const buttonIcons = [{element: component1}, {element: component2}];

  return (
    <View style={{marginHorizontal: 10}}>
      <ButtonGroup
        onPress={setSelectedIndex}
        selectedIndex={selectedIndex}
        buttons={buttons}
        containerStyle={{height: 100}}
      />

      <ButtonGroup
        onPress={setSelectedIndex}
        selectedIndex={selectedIndex}
        buttons={buttons}
        containerStyle={{height: 52, borderColor: 'red', borderRadius: 8}}
        selectedButtonStyle={{backgroundColor: 'cyan'}}
        textStyle={{color: 'green'}}
      />

      <ButtonGroup
        onPress={setSelectedIndex}
        selectedIndex={selectedIndex}
        buttons={buttons}
        containerStyle={{
          height: 100,
          borderRadius: 12,
          borderColor: 'grey',
          backgroundColor: 'white',
        }}
        selectedButtonStyle={{
          backgroundColor: 'cyan',
          borderColor: 'green',
          borderWidth: 1.5,
        }}
        selectedTextStyle={{color: 'yellow'}}
        textStyle={{color: 'blue'}}
        vertical={true}
      />

      <ButtonGroup
        onPress={setSelectedIndex}
        selectedIndex={selectedIndex}
        buttons={buttonIcons}
        containerStyle={{height: 52}}
      />
    </View>
  );
};

const style = StyleSheet.create({
  buttonContainer: {
    marginTop: 10,
  },
});
