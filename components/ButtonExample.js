import React from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';
import {Button} from 'react-native-elements';
import Icon from 'react-native-vector-icons/FontAwesome';

export default App => {
  const alrtFunc = title => {
    console.log(title);
  };
  return (
    <ScrollView>
      <View style={{marginHorizontal: 10}}>
        <View style={style.buttonContainer}>
          <Button title="Solid Button" />
        </View>

        <View style={style.buttonContainer}>
          <Button
            title="Solid Button"
            containerStyle={{
              borderWidth: 10,
              borderColor: 'yellow',
              borderRadius: 8,
            }}
            buttonStyle={{
              backgroundColor: 'red',
            }}
            titleStyle={{color: 'black'}}
          />
        </View>

        <View style={style.buttonContainer}>
          <Button title="Outline button" type="outline" />
        </View>

        <View style={style.buttonContainer}>
          <Button title="Clear button" type="clear" />
        </View>

        <View style={style.buttonContainer}>
          <Button
            icon={
              <Icon
                name="arrow-left"
                size={15}
                color="white"
                style={{marginEnd: 10}}
              />
            }
            title="Button with icon component"
          />
        </View>

        <View style={style.buttonContainer}>
          <Button
            icon={{
              name: 'arrow-right',
              size: 15,
              color: 'white',
              marginStart: 10,
            }}
            title="Button with icon object"
            disabled
          />
        </View>

        <View style={style.buttonContainer}>
          <Button
            icon={
              <Icon
                name="arrow-right"
                size={15}
                color="yellow"
                style={{marginStart: 10}}
              />
            }
            iconRight
            title="Button with right icon"
          />
        </View>

        <View style={style.buttonContainer}>
          <Button
            title="Loading button"
            onPress={() => alrtFunc('Loading button test ')}
          />
        </View>

        <View style={style.roundButtonContainer}>
        
        </View>
      </View>
    </ScrollView>
  );
};

const style = StyleSheet.create({
  buttonContainer: {
    marginTop: 30,
  },
  roundButtonContainer: {
    marginTop: 30,
    height: 70,
    width: 70,
    justifyContent:"center",
    alignItems:"center",
    padding:10,
    borderRadius:35,
    backgroundColor:'orange'
  },
});
