import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import ButtonExample from './components/ButtonExample';
import ButtonGroupExample from './components/ButtonGroupExample';
import Ionicons from 'react-native-vector-icons/Ionicons';

export default App => {
  return (
    <NavigationContainer>
      <Tab.Navigator
        screenOptions={({route}) => ({
          tabBarIcon: ({focused}) => {
            let iconName;
            let color;

            if (route.name === 'Button') {
              iconName = focused
                ? 'ios-information-circle'
                : 'ios-information-circle-outline';
              color = focused ? 'green' : 'black';
            } else if (route.name === 'ButtonGroup') {
              iconName = focused ? 'ios-list-circle' : 'ios-list';
              color = focused ? 'green' : 'black';
            }

            return <Ionicons name={iconName} size={24} color={color} />;
          },
          tabBarActiveTintColor: 'green',
          tabBarInactiveTintColor: 'red',
          tabBarShowLabel: true,
        })}>
        <Tab.Screen
          name="Button"
          component={ButtonExample}
          options={{
            tabBarLabel: 'Button Eg',
            title: 'Button Example',
            tabBarLabelStyle: {fontSize: 12},
            headerTitleAlign: 'center',
            tabBarBadge: 3
          }}
        />

        <Tab.Screen
          name="ButtonGroup"
          component={ButtonGroupExample}
          options={{
            tabBarLabel: 'ButtonGroup Eg',
            tabBarLabelStyle: {fontSize: 12},
            title: 'ButtonGroup Example',
            headerTitleAlign: 'left',
          }}
        />
      </Tab.Navigator>
    </NavigationContainer>
  );
};

const Tab = createBottomTabNavigator();
